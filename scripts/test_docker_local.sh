#!/bin/bash

set -ex

docker build -t devops:test /home/vagrant/lab2/devops-practica2/app/.

cid=$(docker run -d -p 6789:5000 devops:test)

until test=$(curl localhost:6789 | grep 'Index!') &> /dev/null
do
    sleep 0.1
done

# Clean Docker
docker rm -fv "$cid" &> /dev/null

if [[ "$test" == "Index!" ]]; then
    echo "Test Passes! Smile :)"
else
    echo "Test Fails, fix it!"
    exit 1
fi
