#!/bin/bash

set -ex

virtualenv .venv

. .venv/bin/activate

pip install -r /home/vagrant/lab2/devops-practica2/app/requirements.txt

coverage run /home/vagrant/lab2/devops-practica2/app/test_app.py

coverage report /home/vagrant/lab2/devops-practica2/app/app.py
